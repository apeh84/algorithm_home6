/*

REF:
https://en.wikipedia.org/wiki/Kruskal%27s_algorithm
 https://www.geeksforgeeks.org/union-find-algorithm-set-2-union-by-rank/
*/

import java.util.*;
import javafx.util.Pair;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {
    
    /** Main method. */
    public static void main (String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }
    
    
    public Vertex find_root(Vertex x){ // get root of a given vertex
        if(x.root == x)
            return x;
        x.root = find_root(x.root);
        return x.root;
    }
    
    /** Actual main method to run examples and everything. */
    public void run() {
        Graph g = new Graph ("G");
        int V = 6;
        int E = 10;
        g.createRandomSimpleGraph (V, E);
        System.out.println (g);
        g.findMST();
    }
    
    
    class Vertex {
        
        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        public Vertex root;
        public int rank = 0;
        // You can add more fields, if needed
        
        Vertex (String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
            root = this;
        }
        
        Vertex (String s) {
            this (s, null, null);
        }
        
        @Override
        public String toString() {
            return id;
        }
        
        // TODO!!! Your Vertex methods here!
    }
    
    
    /** Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {
        
        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        public int weight;
        public Vertex source;
        // You can add more fields, if needed
        
        Arc (String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }
        
        Arc (String s) {
            this (s, null, null);
        }
        
        @Override
        public String toString() {
            return "" + id + "(" + weight + ")";
        }
        
    }
    
    /*
     * Java Implementation of Kruskal's Algorithm using disjoing sets
     * Works on UN-directed graphs.
     */
    
    class Graph {
        
        private String id;
        private Vertex first;
        private int info = 0;
        public List<Arc> arcList = new ArrayList();
        private int MAX_WEIGHT = 100;
        
        private int vertexSize;
        // You can add more fields, if needed
        
        Graph (String s, Vertex v) {
            id = s;
            first = v;
        }
        
        Graph (String s) {
            this (s, null);
        }
        
        @Override
        public String toString() {
            String nl = System.getProperty ("line.separator");
            StringBuffer sb = new StringBuffer (nl);
            sb.append (id);
            sb.append (nl);
            Vertex v = first;
            while (v != null) {
                sb.append (v.toString());
                sb.append (" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append (" ");
                    sb.append (a.toString());
                    sb.append (" (");
                    sb.append (v.toString());
                    sb.append ("->");
                    sb.append (a.target.toString());
                    sb.append (")");
                    a = a.next;
                }
                sb.append (nl);
                v = v.next;
            }
            return sb.toString();
        }
        
        public Vertex createVertex (String vid) {
            Vertex res = new Vertex (vid);
            res.next = first;
            first = res;
            vertexSize++;
            return res;
        }
        
        public Arc createArc (String aid, Vertex from, Vertex to, int weight) {
            Arc res = new Arc (aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            res.weight = weight;
            res.source = from;
            arcList.add(res);
            return res;
        }
        
        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         * @param n number of vertices added to this graph
         */
        public void createRandomTree (int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex [n];
            for (int i = 0; i < n; i++) {
                varray [i] = createVertex ("v" + String.valueOf(n-i));
                if (i > 0) {
                    int vnr = (int)(Math.random()*i);
                    createArc ("a" + varray [vnr].toString() + "_"
                               + varray [i].toString(), varray [vnr], varray [i], (int)(Math.random() * MAX_WEIGHT));
                    createArc ("a" + varray [i].toString() + "_"
                               + varray [vnr].toString(), varray [i], varray [vnr], (int)(Math.random() * MAX_WEIGHT));
                } else {}
            }
        }
        
        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int [info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res [i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }
        
        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph (int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException ("Too many vertices: " + n);
            if (m < n-1 || m > n*(n-1)/2)
                throw new IllegalArgumentException
                ("Impossible number of edges: " + m);
            first = null;
            createRandomTree (n);       // n-1 edges created here
            Vertex[] vert = new Vertex [n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int)(Math.random()*n);  // random source
                int j = (int)(Math.random()*n);  // random target
                if (i==j)
                    continue;  // no loops
                if (connected [i][j] != 0 || connected [j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert [i];
                Vertex vj = vert [j];
                createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj, (int)(Math.random() * MAX_WEIGHT));
                connected [i][j] = 1;
                createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi, (int)(Math.random() * MAX_WEIGHT));
                connected [j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }
        
        
        /**
         * find MST using kruskal algorithm
         * This will output arcs in the standard output.
         * MST is three so there are V-1 arcs in the otuput.
         * Each arc will be represented the start and end points and the weight in parenthesis
         */
        void findMST(){
            
            // sort edges in increasing order.
            for(int i = 0; i < arcList.size(); i++){
                for(int j = 0; j < i; j++) if(arcList.get(i).weight < arcList.get(j).weight){
                    Arc tmp = arcList.get(j);
                    arcList.set(j, arcList.get(i));
                    arcList.set(i, tmp);
                }
            }
            
            System.out.println("MST");
            int edgecount = 0;
            for(Arc a : arcList){
                
                Vertex ra = find_root(a.source);
                Vertex rb = find_root(a.target);
                
                if(ra != rb){ // If two end point of edge is not in the same set
                    // we need to union it
                    if(ra.rank < rb.rank)
                        ra.root = rb;
                    else if(rb.rank < ra.rank)
                        rb.root = ra;
                    else{
                        rb.root = ra;
                        ra.rank ++;
                    }
                    // print edges in the standard output
                    System.out.println(a.source + "->" + a.target + "(" + a.weight + ")");
                    ++edgecount;
                    if(edgecount == vertexSize - 1) // Tree is completed, because tree has V-1 edges
                        break;
                }
            }
        }
    }
}

